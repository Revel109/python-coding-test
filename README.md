## Python Coding Test

## cd Problem-n folder

### Problem 1
* run `python3 nested_dict_depth_count.py`

* >`python3 -m unittest discover`

### cd Problem-n folder

### Problem 2
* run `python3 nested_dict_depth_checker.py`
 
* >`python3 -m unittest discover`

### cd Problem-n folder

### Problem 3
* run `python3 find_lca.py`

* >`python3 -m unittest discover`

## Time and memory complexity:
Time complexity of the above solution is O(n).
It's just operated a simple traversal.