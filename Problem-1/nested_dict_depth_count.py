lst = []

def nested_dict_depth(dictionary: dict , level: int=0)-> list:
    for key, value in dictionary.items():
        lst.append(f'{key} {level + 1}')
        if isinstance(value, dict): # Checking the value is a Dictionary
            nested_dict_depth(value, level=level+1)
    
    return lst

if __name__ == "__main__":
    a = {
    'key1': 1,
    'key2': {
        'key3': 1,
        'key4': {
            'key5': 4
            }
        }
    }
    
    nested_dict_depth(a)