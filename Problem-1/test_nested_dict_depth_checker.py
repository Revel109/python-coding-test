from unittest import TestCase
from unittest.mock import patch

import app

class ProblemATest(TestCase):
    """
    Unit test class for Problem A
    """
    def setUp(self):
        """ Executed before every test case """
        self.sample_input = app.a
        self.sample_output = ['key1 1', 'key2 1', 'key3 2', 'key4 2', 'key5 3']
    
    def test_nested_dict_depth_checker(self):
        with patch('builtins.print') as mocked_print:
            lst = app.nested_dict_depth(self.sample_input)
            
            for i in range(5):
                print(lst[i])
                mocked_print.assert_called_with(self.sample_output[i])

# Execute all the tests when the file is executed
if __name__ == "__main__":
    unittest.main()