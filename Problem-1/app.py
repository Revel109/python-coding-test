from nested_dict_depth_count import  nested_dict_depth

a = {
    'key1': 1,
    'key2': {
        'key3': 1,
        'key4': {
            'key5': 4
            }
        }
    }

if __name__ == "__main__":
    lst = nested_dict_depth(a)

    print(*lst, sep='\n')