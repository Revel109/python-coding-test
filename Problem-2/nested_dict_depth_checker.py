class Person:
    def __init__(self, first_name, last_name, father):
        self.first_name = first_name
        self.last_name = last_name
        self.father = father
    
    def __iter__(self):
        return iter([('first_name', self.first_name),
                     ('last_name', self.last_name),
                     ('father', self.father)
                    ])    

lst = []

def nested_dict_depth_checker(dictionary: dict , level: int=0)-> list:
    """Prints the depth level of each dictionary key.
        Parameters
        ----------
        dictionary : dict, required
            dictionary who's key: value pair can be key: class_object

        level : int, Optional
            The track the depth level of each key(default is None)
    """

    for key, value in dictionary.items():
        lst.append(f"{key} {level + 1}")
        # print(key, level + 1)
        # TODO: Strictly checking Person Object. Generic checking would be best.
        if isinstance(value, Person):
            value = vars(value) ## vars will convert a class object to dictionary or dict() can be used too.   
        if isinstance(value, dict): ## Checking the value is a dictionary
            nested_dict_depth_checker(value, level=level + 1)
    
    return lst


if __name__ == "__main__":
    person_a = Person('User', '1', None)
    person_b = Person('User', '2', person_a)

    a = {
    'key1': 1,
    'key2': {
        'key3': 1,
        'key4': {
            'key5': 4,
            'user': person_b,
            }
        },
    }
    
    nested_dict_depth_checker(a)
    print(*lst)