from nested_dict_depth_checker import Person, nested_dict_depth_checker

person_a = Person('User', '1', None)
person_b = Person('User', '2', person_a)

a = {
'key1': 1,
'key2': {
    'key3': 1,
    'key4': {
        'key5': 4,
        'user': person_b,
        }
    },
}

if __name__ == "__main__":
    lst = nested_dict_depth_checker(a)

    print(*lst, sep='\n')