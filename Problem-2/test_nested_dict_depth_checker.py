from unittest import TestCase
from unittest.mock import patch

import app

class ProblemBTest(TestCase):
    """
    Unit test class for Problem B
    """
    def setUp(self):
        """ Executed before every test case """
        self.person_a = app.person_a
        self.person_b = app.person_b
        
        self.sample_input = app.a
        self.sample_output = ['key1 1', 'key2 1', 'key3 2', 'key4 2', 'key5 3', 'user 3', 'first_name 4', 'last_name 4', 'father 4', 'first_name 5', 'last_name 5', 'father 5']
    
    def test_nested_dict_depth_checker(self):
        with patch('builtins.print') as mocked_print:
            lst = app.nested_dict_depth_checker(self.sample_input)
            
            for i in range(12):
                print(lst[i])
                mocked_print.assert_called_with(self.sample_output[i])

# Execute all the tests when the file is executed
if __name__ == "__main__":
    unittest.main()