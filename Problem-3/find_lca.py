class Node:
    def __init__(self, key):
        self.key = key
        self.left = None
        self.right = None


def find_lca(root, node1, node2):
    if not root:
        return None

    if root.key == node1 or root.key == node2: 
        return root

    left_lca = find_lca(root.left, node1, node2) 
    right_lca = find_lca(root.right, node1, node2)

    if left_lca and right_lca: 
        return root  

    return left_lca if left_lca is not None else right_lca


if __name__ == '__main__':
    root = Node(1)
    root.left = Node(2)
    root.right = Node(3)
    root.left.left = Node(4)
    root.left.right = Node(5)
    root.right.left = Node(6)
    root.right.right = Node(7)
    root.left.left.left = Node(8)
    root.left.left.right = Node(9)

    print ("LCA of 4 and 5 = ", find_lca(root, 4, 5).key)
    print ("LCA of 7 and 8 = ", find_lca(root, 7, 8).key)
    print ("LCA of 6 and 7 = ", find_lca(root, 6, 7).key)